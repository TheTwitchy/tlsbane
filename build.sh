#!/bin/bash

# This is the main build script for tlsbane. It is intended to be run as is, but can be modified to only install certain classes of clients, by editing the very bottom.

source common.sh

function native (){
	cecho -c "invert" "## Installing Linux native command line clients..."
	cecho -c "invert" "### Installing curl..."
	sudo apt install -qq -y curl
    cecho -c "invert" "#### Done."

	cecho -c "invert" "### Installing wget..."
	sudo apt install -qq -y wget
    cecho -c "invert" "#### Done."
}

function ccpp() {
    cd ccpp
	( exec "./build.sh" )
    cd ..
}

function py() {
    cd py
    ( exec "./build.sh" )
    cd ..
}

function go() {
    cd go
    ( exec "./build.sh" )
    cd ..
}


cecho -c "invert" "# Installing SSL/TLS clients..."

# If you want to limit what clients get installed, edit them out here.
native
ccpp
py
go

cecho -c "invert" "# SSL/TLS clients Installed (probably)"
