#!/bin/bash

source ../common.sh

cecho -c "invert" "## Building Golang clients and thier harnesses" 
cecho -c "invert" "### Installing Golang common dependencies"
sudo apt install -qq -y golang-go

cd gohttp
( exec "./build.sh" )
cd ..

