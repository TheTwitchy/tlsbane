#!/bin/bash
source ../../common.sh

# libcurlopenssl
cecho -c "invert" "### Making golang HTTP harness..."
cecho -c "invert" "#### Installing dependencies..."
cecho -c "invert" "#### Building gohttp_harness..."
go build gohttp_harness.go
cecho -c "invert" "#### Done."