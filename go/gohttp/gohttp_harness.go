package main

import "fmt"
import "net/http"
import "crypto/tls"
import "flag"
import "os"
import "runtime"


func main() {
    url := os.Args[len(os.Args) - 1]
    insecureFlag := flag.Bool("insecure", false, "disable server certificate verfication")
    versionFlag := flag.Bool("version", false, "show golang version and exit")
    flag.Parse()
    
    if *versionFlag {
        fmt.Println("golang " + runtime.Version())
        os.Exit(0)
    }

    if *insecureFlag {
        http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
    } else {
        http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: false}
    }

    _, err := http.Get(url)

    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }
    os.Exit(0)
}
