# Various helper functions and variables

# From https://bytefreaks.net/gnulinux/bash/cecho-a-function-to-print-using-different-colors-in-bash
# The following function prints a text using custom color
# -c or --color define the color for the print. See the array colors for the available options.
# -n or --noline directs the system not to print a new line after the content.
# Last argument is the message to be printed.
function cecho () {
    declare -A colors;
    colors=(\
        ['black']='\E[0;47m'\
        ['red']='\E[0;31m'\
        ['green']='\E[0;32m'\
        ['yellow']='\E[0;33m'\
        ['blue']='\E[0;34m'\
        ['magenta']='\E[0;35m'\
        ['cyan']='\E[0;36m'\
        ['white']='\E[0;37m'\
        ['whiteb']='\E[1;37m'\
        ['invert']='\E[1;7m'\
    );

    local defaultMSG="No message passed.";
    local defaultColor="black";
    local defaultNewLine=true;

    while [[ $# -gt 1 ]];
    do
    key="$1";

    case $key in
        -c|--color)
            color="$2";
            shift;
        ;;
        -n|--noline)
            newLine=false;
        ;;
        *)
            # unknown option
        ;;
    esac
    shift;
    done

    message=${1:-$defaultMSG};   # Defaults to default message.
    color=${color:-$defaultColor};   # Defaults to default color, if not specified.
    newLine=${newLine:-$defaultNewLine};

    echo -en "${colors[$color]}";
    echo -en "$message";
    if [ "$newLine" = true ] ; then
        echo;
    fi
    tput sgr0; #  Reset text attributes to normal without clearing screen.

    return;
}

# These functions will generate a random identifier so that we can easily tie each request with the tool that called it.
function gen_id (){
    openssl rand -hex 2
}

# This is the main function that runs the clients and the various harnesses, with secure connections only
# ARGS
#   $1: The basic name of the client under test, used exclusively for error reporting.
#   $2: The version call that prints out the name and version of the client being tested.
#   $3: The client call to the HTTPS endpoint, with certificate validation enabled.
function client_test_secure () {
    $2 1>/dev/null 2>/dev/null
    if [ $? == 127 ]
    then
        cecho -n -c "white" "## ["
        cecho -n -c "yellow" "WARNING"
        cecho -n -c "white" "] - Client '"
        cecho -n -c "yellow" "$1"
        cecho -n -c "white" "' not available.\n"
    else
        FAILED=false
        TEST_ID=`gen_id`
        FQ_ENDPOINT=$4/$TEST_ID
        CLIENT=`$2 2>/dev/null | head -n 1`
        cecho -n -c "white" "## ["
        cecho -n -c "cyan" "$TEST_ID"
        cecho -n -c "white" "] ["
        cecho -n -c "green" "SECURE"
        cecho -n -c "white" "] ["
        $3 $FQ_ENDPOINT 1>/dev/null 2>tmp.out
        if [ $? == 0 ]
        then
            # No errors reported, therefore the connection went through as normal.
            cecho -n -c "blue" "CONNECTED"
        else
            # The connection failed, the connection attempt was likely terminated.
            cecho -n -c "red" "CONNECTION FAILED"
            FAILED=true
        fi
        cecho -n -c "white" "] "
        cecho -n -c "white" "$CLIENT \n"
        if [ FAILED ]
        then
            cat tmp.out
        fi
        rm -f tmp.out
    fi
}

# This is the main function that runs the clients and the various harnesses, with insecure connections only
# ARGS
#   $1: The basic name of the client under test, used exclusively for error reporting.
#   $2: The version call that prints out the name and version of the client being tested.
#   $3: The client call to the HTTPS endpoint, with certificate validation disabled.
#   $4: The Base endpoint to which to connect, in the form "https://sub.example.com"
function client_test_insecure () {
    $2 1>/dev/null 2>/dev/null
    if [ $? == 127 ]
    then
        cecho -n -c "white" "## ["
        cecho -n -c "yellow" "WARNING"
        cecho -n -c "white" "] - Client '"
        cecho -n -c "yellow" "$1"
        cecho -n -c "white" "' not available.\n"
    else
        FAILED=false
        TEST_ID=`gen_id`
        FQ_ENDPOINT=$4/$TEST_ID
        CLIENT=`$2 2>/dev/null | head -n 1`
        cecho -n -c "white" "## ["
        cecho -n -c "cyan" "$TEST_ID"
        cecho -n -c "white" "] ["
        cecho -n -c "magenta" "INSECURE"
        cecho -n -c "white" "] ["
        $3 $FQ_ENDPOINT 1>/dev/null 2>tmp.out
        if [ $? == 0 ]
        then
            # No errors reported, therefore the connection went through as normal.
            cecho -n -c "blue" "CONNECTED"
        else
            # The connection failed, the connection attempt was likely terminated.
            cecho -n -c "red" "CONNECTION FAILED"
            FAILED=true
        fi
        cecho -n -c "white" "] "
        cecho -n -c "white" "$CLIENT \n"

        if [ FAILED ]
        then
            cat tmp.out
        fi
        rm -f tmp.out
    fi
}

