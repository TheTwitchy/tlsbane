#!/bin/bash

source ../common.sh

cecho -c "invert" "## Building C/C++ Clients and thier harnesses" 

cd libcurlgnutls
( exec "./build.sh" )
cd ..

cd libcurlnss
( exec "./build.sh" )
cd ..

cd libcurlopenssl
( exec "./build.sh" )
cd ..