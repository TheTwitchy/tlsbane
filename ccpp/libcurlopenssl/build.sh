#!/bin/bash
source ../../common.sh

# libcurlopenssl
cecho -c "invert" "### Making libcurl-openssl client libcurlopenssl_harness..."
cecho -c "invert" "#### Installing dependencies..."
sudo apt install -qq -y libcurl4-openssl-dev
sudo apt-mark manual libcurl4-openssl-dev
cecho -c "invert" "#### Building libcurlopenssl_harness..."
gcc -o libcurlopenssl_harness libcurlopenssl_harness.c -l curl
cecho -c "invert" "#### Done."