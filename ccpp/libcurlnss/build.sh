#!/bin/bash
source ../../common.sh

# libcurlnss
cecho -c "invert" "### Making libcurl-nss client libcurlnss_harness..."
cecho -c "invert" "#### Installing dependencies..."
sudo apt install -qq -y libcurl4-nss-dev
sudo apt-mark manual libcurl4-nss-dev
cecho -c "invert" "#### Building libcurlnss_harness..."
gcc -o libcurlnss_harness libcurlnss_harness.c -l curl-nss
cecho -c "invert" "#### Done."