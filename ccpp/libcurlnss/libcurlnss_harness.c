#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <curl/curl.h>

#define PROG_NAME "libcurlnss_harness"

int main(int argc, char ** argv){
	int quietFlag = 0;
	int insecureFlag = 0;
  	int c;

	/* Parse option flags */
	while ((c = getopt (argc, argv, "qiv")) != -1){
    		switch (c){
			case 'v':
				fprintf(stdout, "%s - %s\n", PROG_NAME, curl_version());
				return 0;
      			case 'q':
				/* Turns off most output */
        			quietFlag = 1;
        			break;
      			case 'i':
				/* Causes the connection to be made insecurely */
        			insecureFlag = 1;
        			break;
		}
	}

	if(!quietFlag){
		fprintf(stdout, "Starting %s test, using '%s'.\n", PROG_NAME, argv[optind]);
	}

	CURL *curl;
    	CURLcode res;
    	curl_global_init(CURL_GLOBAL_DEFAULT);

    	curl = curl_easy_init();
    	if(curl) {
        	curl_easy_setopt(curl, CURLOPT_URL, argv[optind]);

		/* Set opt to write the recvd data to nowhere.*/
		FILE *f = fopen("/dev/null", "wb");

		curl_easy_setopt(curl, CURLOPT_WRITEDATA, f);

        	if (insecureFlag){
            		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
            		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
        	}

        	/* Perform the request, res will get the return code */
        	res = curl_easy_perform(curl);

        	/* Check for errors */
        	if(res != CURLE_OK){
            		fprintf(stderr, "Error in %s, connection failed: %s\n", PROG_NAME, curl_easy_strerror(res));
			curl_easy_cleanup(curl);
			curl_global_cleanup();
			return -3;
        	}

		if (!quietFlag){
			fprintf(stdout, "Successful connection in %s.\n", PROG_NAME);
		}
        	curl_easy_cleanup(curl);
        	curl_global_cleanup();
		return 0;
	}
   	else {
        	curl_global_cleanup();
        	return -2;
    	}
}
