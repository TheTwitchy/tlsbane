#!/bin/bash
source ../../common.sh

# libcurlgnutls
cecho -c "invert" "### Making libcurlgnutls client libcurlgnutls_harness..."
cecho -c "invert" "#### Installing dependencies..."
sudo apt install -qq -y libcurl4-gnutls-dev
sudo apt-mark manual libcurl4-gnutls-dev
cecho -c "invert" "#### Building libcurlgnutls_harness..."
gcc -o libcurlgnutls_harness libcurlgnutls_harness.c -l curl-gnutls
cecho -c "invert" "#### Done."