#!/bin/bash

source ../common.sh

cecho -c "invert" "## Building Python clients and thier harnesses" 
cecho -c "invert" "### Installing Python common dependencies"
# Each harness runs in it's own virtualenv, this does away with the problem of having multiple harnesses that all rely on teh same things, like the C/C++ harnesses.
sudo apt install -qq -y python3-pip virtualenv

cd requests
( exec "./build.sh" )
cd ..

cd urllib3
( exec "./build.sh" )
cd ..