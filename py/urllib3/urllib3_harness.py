#!/usr/bin/env python3

import argparse, sys, urllib3, certifi

VERSION = urllib3.__version__
PROG_NAME = "urllib3_harness"
LIB_NAME = "urllib3"

def parse_args():
    #Setup the argparser and all args
    parser = argparse.ArgumentParser(prog=PROG_NAME)
    parser.add_argument("-v", "--version", action="version", version="%s - %s v%s" % (PROG_NAME, LIB_NAME, VERSION))
    parser.add_argument("-q", "--quiet", help="surpress extra output", action="store_true", default=False)
    parser.add_argument("-i", "--insecure", help="make call without certificate validation", action="store_true", default=False)
    parser.add_argument("endpoint", help="the URL to call")
    return parser.parse_args()

if __name__ == "__main__":
    argv = parse_args()
    try:
        # Otherwise we get annoying ouput
        urllib3.disable_warnings()

        # From https://urllib3.readthedocs.io/en/latest/user-guide.html#ssl, this installs the Mozilla root CA bundle (via the certifi package). There is also a cert bundle at /etc/ssl/certs/ca-certificates.crt, but we probably don't need both.
        if argv.insecure:
            http = urllib3.PoolManager(ca_certs=certifi.where())
        else:
            http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=certifi.where())

        r = http.request('GET', argv.endpoint)

        if not argv.quiet:
            sys.stderr.write("Connection was successful.\n")
    except Exception as e:
        sys.stderr.write("Failed to make connection. %s\n" % str(e))
        quit(-1)
    quit(0)