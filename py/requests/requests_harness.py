#!/usr/bin/env python3

import requests, argparse, sys, urllib3

VERSION = requests.__version__
PROG_NAME = "requests_harness"
LIB_NAME = "requests"

def parse_args():
    #Setup the argparser and all args
    parser = argparse.ArgumentParser(prog=PROG_NAME)
    parser.add_argument("-v", "--version", action="version", version="%s - %s v%s" % (PROG_NAME, LIB_NAME, VERSION))
    parser.add_argument("-q", "--quiet", help="surpress extra output", action="store_true", default=False)
    parser.add_argument("-i", "--insecure", help="make call without certificate validation", action="store_true", default=False)
    parser.add_argument("endpoint", help="the URL to call")
    return parser.parse_args()

if __name__ == "__main__":
    argv = parse_args()
    try:
        # Otherwise we get annoying ouput
        urllib3.disable_warnings()

        r = requests.get(argv.endpoint, verify=not argv.insecure)
        if not argv.quiet:
            sys.stderr.write("Connection was successful.\n")
    except Exception as e:
        sys.stderr.write("Failed to make connection. %s\n" % str(e))
        quit(-1)
    quit(0)