#!/bin/bash
source ../../common.sh

# libcurlnss
cecho -c "invert" "### Making Python requests client requests_harness..."
cecho -c "invert" "#### Installing dependencies..."

if [ ! -d "venv" ]
then
    virtualenv --python=python3 venv
fi
. ./venv/bin/activate
pip3 install -r requirements.txt

cecho -c "invert" "#### Done."