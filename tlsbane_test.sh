#!/bin/bash

source common.sh

# Thanks http://patorjk.com/software/taag/#p=display&f=ANSI%20Shadow&t=tlsbane
cecho -n -c "white" "████████╗██╗     ███████╗██████╗  █████╗ ███╗   ██╗███████╗\n"
cecho -n -c "white" "╚══██╔══╝██║     ██╔════╝██╔══██╗██╔══██╗████╗  ██║██╔════╝\n"
cecho -n -c "white" "   ██║   ██║     ███████╗██████╔╝███████║██╔██╗ ██║█████╗  \n"
cecho -n -c "white" "   ██║   ██║     ╚════██║██╔══██╗██╔══██║██║╚██╗██║██╔══╝  \n"
cecho -n -c "white" "   ██║   ███████╗███████║██████╔╝██║  ██║██║ ╚████║███████╗\n"
cecho -n -c "white" "   ╚═╝   ╚══════╝╚══════╝╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝\n"
cecho -n -c "whiteb"  "                                             by TheTwitchy \n"

# Define the base endpoint to which every client will attempt to connect. Use something with a subdomain if possible, as certain attacks rely on it.
if [ -z "$1" ]
then
    cecho -n -c "white" "## ["
    cecho -n -c "red" "ERROR"
    cecho -n -c "white" "] - Enter the base URL on the command line, in the form '"
    cecho -n -c "yellow" "https://example.com"
    cecho -n -c "white" "'.\n"
    exit 1
fi
BASE_ENDPOINT=$1 

# Curl
client_test_secure "curl" "curl --version | head -n1" "curl -s" "$BASE_ENDPOINT"
client_test_insecure "curl" "curl --version | head -n1" "curl -k -s" "$BASE_ENDPOINT"

# wget
client_test_secure "wget" "wget --version | head -n 1" "wget --quiet -O- " "$BASE_ENDPOINT"
client_test_insecure "wget" "wget --version | head -n 1" "wget --quiet -O- --no-check-certificate" "$BASE_ENDPOINT"

# libcurlgnutls
client_test_secure "libcurlgnutls" "./ccpp/libcurlgnutls/libcurlgnutls_harness -v" "./ccpp/libcurlgnutls/libcurlgnutls_harness -q" "$BASE_ENDPOINT"
client_test_insecure "libcurlgnutls" "./ccpp/libcurlgnutls/libcurlgnutls_harness -v" "./ccpp/libcurlgnutls/libcurlgnutls_harness -q -i" "$BASE_ENDPOINT"

# libcurlnss
client_test_secure "libcurlnss" "./ccpp/libcurlnss/libcurlnss_harness -v" "./ccpp/libcurlnss/libcurlnss_harness -q" "$BASE_ENDPOINT"
client_test_insecure "libcurlnss" "./ccpp/libcurlnss/libcurlnss_harness -v" "./ccpp/libcurlnss/libcurlnss_harness -q -i" "$BASE_ENDPOINT"

# libcurlopenssl
client_test_secure "libcurlopenssl" "./ccpp/libcurlopenssl/libcurlopenssl_harness -v" "./ccpp/libcurlopenssl/libcurlopenssl_harness -q" "$BASE_ENDPOINT"
client_test_insecure "libcurlopenssl" "./ccpp/libcurlopenssl/libcurlopenssl_harness -v" "./ccpp/libcurlopenssl/libcurlopenssl_harness -q -i" "$BASE_ENDPOINT"

# python requests
source ./py/requests/venv/bin/activate
client_test_secure "python requests" "./py/requests/requests_harness.py -v" "./py/requests/requests_harness.py -q" "$BASE_ENDPOINT"
client_test_insecure "python requests" "./py/requests/requests_harness.py -v" "./py/requests/requests_harness.py -q -i" "$BASE_ENDPOINT"
deactivate 

# python urllib3
source ./py/urllib3/venv/bin/activate
client_test_secure "python urllib3" "./py/urllib3/urllib3_harness.py -v" "./py/urllib3/urllib3_harness.py -q" "$BASE_ENDPOINT"
client_test_insecure "python urllib3" "./py/urllib3/urllib3_harness.py -v" "./py/urllib3/urllib3_harness.py -q -i" "$BASE_ENDPOINT"
deactivate 

# golang net/http
client_test_secure "golang net/http" "./go/gohttp/gohttp_harness -version" "./go/gohttp/gohttp_harness" "$BASE_ENDPOINT"
client_test_insecure "golang net/http" "./go/gohttp/gohttp_harness -version" "./go/gohttp/gohttp_harness -insecure" "$BASE_ENDPOINT"

# Non-installed client test to test failure case.
client_test_secure "noclient_test" "noclient_test --version" "noclient_test --quiet" "$BASE_ENDPOINT"
client_test_insecure "noclient_test" "noclient_test --version" "noclient_test --quiet" "$BASE_ENDPOINT"