# tlsbane
Making the Internet a more dangerous place. This is a library intended to make an HTTPS connection with every concievable HTTPS client I can think of. This enables fast testing of SSL/TLS hijacking attacks.

## Usage
* `tlsbane.sh <HTTPS URL ENDPOINT>`
    * tlsbane will make a call to the endpoint as given on the command line, and append a 4 digit hex identifier so that any hits can be correlated immediately.
    * It's recommended to use a site that always returns a `200 OK`, as some clients may interpret a 404 as an error, and report it as such. The application at https://gitlab.com/TheTwitchy/test is a good example of something that follows this pattern, but other sites may be used.

## Example Run
```
root@ubuntu:/tlsbane$ ./tlsbane_test.sh https://test.thetwitchy.com
████████╗██╗     ███████╗██████╗  █████╗ ███╗   ██╗███████╗
╚══██╔══╝██║     ██╔════╝██╔══██╗██╔══██╗████╗  ██║██╔════╝
   ██║   ██║     ███████╗██████╔╝███████║██╔██╗ ██║█████╗  
   ██║   ██║     ╚════██║██╔══██╗██╔══██║██║╚██╗██║██╔══╝  
   ██║   ███████╗███████║██████╔╝██║  ██║██║ ╚████║███████╗
   ╚═╝   ╚══════╝╚══════╝╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝
                                             by TheTwitchy 
## [3ddd] [SECURE] [CONNECTED] curl 7.58.0 (x86_64-pc-linux-gnu) libcurl/7.58.0 OpenSSL/1.1.0g zlib/1.2.11 libidn2/2.0.4 libpsl/0.19.1 (+libidn2/2.0.4) nghttp2/1.30.0 librtmp/2.3 
## [18d3] [INSECURE] [CONNECTED] curl 7.58.0 (x86_64-pc-linux-gnu) libcurl/7.58.0 OpenSSL/1.1.0g zlib/1.2.11 libidn2/2.0.4 libpsl/0.19.1 (+libidn2/2.0.4) nghttp2/1.30.0 librtmp/2.3 
## [69f5] [SECURE] [CONNECTED] GNU Wget 1.19.4 built on linux-gnu. 
## [406a] [SECURE] [CONNECTED] GNU Wget 1.19.4 built on linux-gnu. 
## [c3b1] [SECURE] [CONNECTED] libcurlgnutls_harness - libcurl/7.58.0 GnuTLS/3.5.18 zlib/1.2.11 libidn2/2.0.4 libpsl/0.19.1 (+libidn2/2.0.4) nghttp2/1.30.0 librtmp/2.3 
## [942a] [INSECURE] [CONNECTED] libcurlgnutls_harness - libcurl/7.58.0 GnuTLS/3.5.18 zlib/1.2.11 libidn2/2.0.4 libpsl/0.19.1 (+libidn2/2.0.4) nghttp2/1.30.0 librtmp/2.3 
## [dec9] [SECURE] [CONNECTION FAILED] libcurlnss_harness - libcurl/7.58.0 NSS/3.35 zlib/1.2.11 libidn2/2.0.4 libpsl/0.19.1 (+libidn2/2.0.4) nghttp2/1.30.0 librtmp/2.3 
Error in libcurlnss_harness, connection failed: Problem with the SSL CA cert (path? access rights?)
## [2dff] [INSECURE] [CONNECTION FAILED] libcurlnss_harness - libcurl/7.58.0 NSS/3.35 zlib/1.2.11 libidn2/2.0.4 libpsl/0.19.1 (+libidn2/2.0.4) nghttp2/1.30.0 librtmp/2.3 
Error in libcurlnss_harness, connection failed: SSL connect error
## [754b] [SECURE] [CONNECTED] libcurlopenssl_harness - libcurl/7.58.0 OpenSSL/1.1.0g zlib/1.2.11 libidn2/2.0.4 libpsl/0.19.1 (+libidn2/2.0.4) nghttp2/1.30.0 librtmp/2.3 
## [a405] [INSECURE] [CONNECTED] libcurlopenssl_harness - libcurl/7.58.0 OpenSSL/1.1.0g zlib/1.2.11 libidn2/2.0.4 libpsl/0.19.1 (+libidn2/2.0.4) nghttp2/1.30.0 librtmp/2.3 
## [644c] [SECURE] [CONNECTED] requests_harness - requests v2.21.0 
## [6cdc] [INSECURE] [CONNECTED] requests_harness - requests v2.21.0 
## [b385] [SECURE] [CONNECTED] urllib3_harness - urllib3 v1.24.1 
## [5ca3] [INSECURE] [CONNECTED] urllib3_harness - urllib3 v1.24.1 
## [WARNING] - Client 'noclient_test' not available.
## [WARNING] - Client 'noclient_test' not available.
```

## Installation
### Environment
* This script is intended to be run in a Ubuntu 18.04 LTS VM (the server edition or mini.iso). Seriously, this installs a ton of shit, you don't want this on your real host. Plus, it's easier to intercept from a VM.

### Install Dependencies and Clients
* Run the following PRIOR to running the build script or installer.
	* `sudo apt-get install build-essential`
* To install all clients, run:
	* `./build.sh`
* To install only certain specific clients or classes of clients, remove the build calls from the hierarchical build.sh files, the test script will auto-detect their availability at runtime and just skip the test if they don't exist

### Run Test Script
* `./tlsbane_test.sh`

## Clients
This is a listing of currently supported HTTPS clients, organized by language.

### CLI
* curl
* wget

### C/C++
* libcurl-gnutls
* libcurl-nss
* libcurl-openssl

### Python
* requests
* urllib3

### Golang
* net/http

## Contributing
* Know of a weird HTTPS client that we can add to the test suite? Add it.
* In general, because we are testing libraries and frameworks, you'll probably have to build a harness to exercise the library. Follow the guidelines as much as possible to keep everything consistent.

### Overall Guidelines
* Follow the standard, most widely accepted, developer practices for whatever language/framework you're building against.
* Isolate dependencies if at all possible (i.e. don't install system packages via apt if you can help it).
* Keep binaries and dependencies out of the repo.

### Harness Guidelines
* The harness program MUST return a 0 on success (connection made with no errors) and anything else on a failure. Failing to do so breaks everything.
* On a failure, plainly print out the reason for the failure if possible. In theory these should always be connection errors, and not anything else (like a general purpose input failure).
* You should have a way to indicate to the harness to connect insecurely (without certificate validation), to suppress non-error output, and to print the version number of the client bring tested. This is all normally implemented via switches passed via command line.
* The harness should take the fully-qualified endpoint as the final non-optional arg on the command line.
* Otherwise just follow the patterns of other harnesses, they're easy to read (py/requests/requests_harness.py might be the easiest to grok).

### Final Checklist After Adding Harness
* Run the entire root build script from a new VM. Ensure any new build.sh scripts are included.
* Run the tlsbane script to ensure there are no errors and that the new harness is exercised.
* Update the README to reflect the newly added client harness.